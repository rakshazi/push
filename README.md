[![GoDoc](https://godoc.org/gitlab.com/rakshazi/push?status.svg)](https://godoc.org/gitlab.com/rakshazi/push)
[![Liberapay](https://img.shields.io/badge/donate-liberapay-yellow.svg?style=for-the-badge)](https://liberapay.com/rakshazi)



This program will send push notifications via Google FCM and/or (in the same time) Apple notification services
and provide you easy to use REST API and/or (in the same time) redis queue.

Example request:
```bash
curl -X POST \
    -H "Content-Type: application/json" \
    -d '{"title":"test", "token":"lJo3Kr4X9s1:APA91bEKvZJRuaiZZMmiRArOGx454yePPHaoLQtqqBI3tiXNA_Evu7uBFqMXbn7xZBfR2r4SDQEOj514vmd4L3Nxe_U_Sw9MdOfe1cOkU-sfn2QPlKzSaEzAinihlzqSCai9_n_lS2oE","body":"newbody", "priority": "high", "sound": "sound.mp3", "transport": "fcm", "data_only": false}' \
    http://localhost
```

## Features

### Listens on

* Redis
* HTTP

on the same time. If one of listeners is broken, the second one will work.

### Sends to

* FCM
* APNS

on the same time

## Configuration

This tool needs a `config.json` file with this minimal structure

```json
{
    "http": {
        "enabled": true,
        "port": 8080,
        "endpoint": "/"
    },
    "fcm": {
        "token": "FCM Server token ('new' server key)"
    }
}
```

You can also specify the config file path with the `-config` flag. For example:

```bash
push -config my/custom/dir/my_config.json
```

Check more configuration options in the [conf.json.dist](config.json.dist)

## Run

### Golang

```bash
go get gitlab.com/rakshazi/push
cd $GOPATH/src/gitlab.com/rakshazi/push
go install
curl https://gitlab.com/rakshazi/push/raw/master/config.json.dist -o config.json
push
```

### Binary

```bash
# Get binary from project -> releases page

curl https://gitlab.com/rakshazi/push/raw/master/config.json.dist -o config.json
push
```

### Docker

```bash
curl https://gitlab.com/rakshazi/push/raw/master/config.json.dist -o config.json
docker run -d -v "$PWD/config.json:/etc/config.json" -p 80:8080 registry.gitlab.com/rakshazi/push

curl http://localhost
```

