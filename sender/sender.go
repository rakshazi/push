package sender

import (
	"encoding/json"
	"errors"
	"log"

	"github.com/NaySoftware/go-fcm"
	"gitlab.com/rakshazi/push/config"
)

// Message structure
type Message struct {
	fcm.NotificationPayload
	Token     string                 `json:"token"`
	Transport string                 `json:"transport"`
	Data      map[string]interface{} `json:"data,omitempty"`
	DataOnly  bool                   `json:"data_only",omitempty"`
}

// Cfg data
var Cfg config.Config

// Parse incoming message
func Parse(jsonString []byte) Message {
	message := Message{
		Transport: "fcm",
	}
	err := json.Unmarshal(jsonString, &message)
	if err != nil {
		log.Println("[sender] cannot parse input message", err)
	}
	if message.Data == nil {
		message.Data = make(map[string]interface{})
	}
	message.Data["title"] = message.Title
	message.Data["body"] = message.Body

	return message
}

// Send push notification
func Send(message Message) error {
	var err error

	if len(message.Token) == 0 {
		err = errors.New("[sender] message has no device token, ignoring it")
		log.Println(err)
		return err
	}

	if message.Transport == "fcm" {
		return sendFcm(Cfg.Fcm.Token, message)
	}

	if message.Transport == "apns" {
		return sendApns(Cfg.Apns.Gateway, Cfg.Apns.Cert, Cfg.Apns.Key, message)
	}
	err = errors.New("[sender] message uses wrong transport:" + message.Transport)
	log.Println(err)

	return err
}
