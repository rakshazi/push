package sender

import (
	"log"

	"github.com/Coccodrillo/apns"
)

func sendApns(gateway string, cert string, key string, message Message) error {
	var err error
	payload := apns.NewPayload()
	payload.Alert = message.Title
	payload.Sound = message.Sound
	apnsMessage := apns.NewPushNotification()
	apnsMessage.DeviceToken = message.Token
	apnsMessage.AddPayload(payload)
	apnsMessage.Set("data", message.Data)
	apnsMessage.Set("body", message.Body)
	client := apns.NewClient(gateway, cert, key)
	status := client.Send(apnsMessage)
	log.Println("[sender/apns] notification status:", status.Success, "(error:", status.Error, ")")
	if status.Error != nil {
		err = status.Error
	}

	return err
}
