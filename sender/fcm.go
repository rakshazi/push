package sender

import (
	"log"

	"github.com/NaySoftware/go-fcm"
)

func sendFcm(token string, message Message) error {
	client := fcm.NewFcmClient(token)
	client.NewFcmMsgTo(message.Token, message.Data)
	client.SetMutableContent(true)
	if !message.DataOnly {
		client.SetNotificationPayload(&fcm.NotificationPayload{
			Title:            message.Title,
			Body:             message.Body,
			Sound:            message.Sound,
			Icon:             message.Icon,
			Badge:            message.Badge,
			Tag:              message.Tag,
			Color:            message.Color,
			ClickAction:      message.ClickAction,
			BodyLocKey:       message.BodyLocKey,
			BodyLocArgs:      message.BodyLocArgs,
			TitleLocKey:      message.TitleLocKey,
			TitleLocArgs:     message.TitleLocArgs,
			AndroidChannelID: message.AndroidChannelID,
		})
	}
	status, err := client.Send()
	log.Println("[sender/fcm] notification status:")
	status.PrintResults()
	return err
}
