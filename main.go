package main

import (
	"flag"
	"log"

	"gitlab.com/rakshazi/push/config"
	"gitlab.com/rakshazi/push/listener"
	"gitlab.com/rakshazi/push/sender"
)

func main() {
	configFile := flag.String("config", "config.json", "Path to the configuration file")
	flag.Parse()
	config, err := config.CreateConfigurationFromFile(*configFile)
	if err != nil {
		log.Fatalf("[main] could not load file: %v", err)
	}
	sender.Cfg = config
	listener.Listen(config)
}
