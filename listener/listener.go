package listener

import (
	"sync"

	. "gitlab.com/rakshazi/push/config"
)

var wg sync.WaitGroup

// Listen for incoming messages
func Listen(config Config) {
	if config.Http.Enabled {
		wg.Add(1)
		go func() {
			listenHTTP(config.Http.Endpoint, config.Http.Port)
			wg.Done()
		}()
	}
	if config.Redis.Enabled {
		wg.Add(2)
		go func() {
			listenRedis(config.Redis.Url, config.Redis.List)
			wg.Done()
		}()
	}

	wg.Wait()
}
